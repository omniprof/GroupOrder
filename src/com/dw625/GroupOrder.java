package com.dw625;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Class that determines the order of presentations
 *
 * @author Ken Fogel
 */
public class GroupOrder {

    public void perform() {
        // create random number generator and seed with nanoTime
        Random randomGenerator = new Random(System.nanoTime());

        // Create an array of 6 ints and fill with 0
        int[] order = new int[6];
        Arrays.fill(order, 0);

        // Loop the number of elemnets in the array
        for (int x = 0; x < order.length; ++x) {
            // If the value at a position is still 0 create a new random number
            while (order[x] == 0) {
                // Create the random number
                int rnd = randomGenerator.nextInt(6) + 1;
                // Use a stream to see if the random number already exists in the array
                if (!IntStream.of(order).anyMatch(y -> y == rnd)) {
                    // If not then assign the random number
                    order[x] = rnd;
                }
            }
        }

        // Let's see the results
        for (int z : order) {
            System.out.println("" + z + "\n");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GroupOrder go = new GroupOrder();
        go.perform();
        System.exit(0);
    }
}
